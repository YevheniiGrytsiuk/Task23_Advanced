﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task23.Controllers;
using System.Web.Mvc;

namespace Task23UnitTest
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void HomeController_Index_ShouldNotReturnNull()
        {
            //arrange
            HomeController controller = new HomeController();
            //act
            ViewResult result = controller.Index() as ViewResult;
            //assert 
            Assert.IsNotNull(result);
        }
    }
}
