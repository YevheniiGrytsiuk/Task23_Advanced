﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task23.Controllers;
using System.Web.Mvc;
using Task23BugLess.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Task23UnitTest
{
    [TestClass]
    public class GuestControllerTest
    {
        private List<Feedback> _feedbacks;
        [TestInitialize]
        public void InitFeedbacks() 
        {
            _feedbacks = ((new GuestController()).Index() as ViewResult).Model as List<Feedback>;
        }
        [TestCleanup]
        public void CleanupFeedbacks() 
        {
            _feedbacks = null;
        }

        [TestMethod]
        public void GuestController_Index_ShouldNotReturnNull()
        {
            //arrange
            GuestController controller = new GuestController();
            //act
            ViewResult result = controller.Index() as ViewResult;
            //assert 
            Assert.IsNotNull(result);

        }
        [TestMethod]
        public void GuestController_Feedback_ShouldNotReturnNull()
        {
            //arrange
            GuestController controller = new GuestController();
            //act
            ViewResult result = controller.Feedback(0) as ViewResult;
            //assert 
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GuestController_FeedbackGet_ShouldReturnFeedback() 
        {
            //arrange
            GuestController controller = new GuestController();
            Feedback[] feedbacksOfBlogWithId4 = new[]
            {
                new Feedback()
                {
                    BlogId = 4,
                    AuthorName = "megakiller_2009",
                    CreationTime = DateTime.Parse("4/25/2021 8:32:27 PM"),
                    Content = "Hello"
                },
                new Feedback()
                {
                    BlogId = 4,
                    AuthorName = "1223",
                    CreationTime = DateTime.Parse("4/25/2021 8:32:27 PM"),
                    Content = "Бобало геній"
                }
            };
            //act
            ViewResult result = controller.Feedback(4) as ViewResult;
            var model = result.ViewData.Model as IEnumerable<Feedback>;
            
            //assert
            Assert.IsNotNull(model);
            CollectionAssert.AreEqual(feedbacksOfBlogWithId4, model.ToArray());
        }
        [TestMethod]
        public void GuestController_FeedbackPost_ShouldCreateNewFeedback() 
        {
            //arrange
            GuestController controller = new GuestController();
            Feedback feedbackToBeAdded = new Feedback { BlogId = 0, AuthorName = "Test", Content = "Test", CreationTime = DateTime.Now };
            //act
            controller.Feedback(feedbackToBeAdded);
            //assert
            CollectionAssert.Contains(_feedbacks.ToArray(), feedbackToBeAdded);
        }
    }
}
