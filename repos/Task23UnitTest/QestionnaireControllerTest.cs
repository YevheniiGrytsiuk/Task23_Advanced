﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Task23BugLess.Controllers;
using Task23BugLess.Helpers;
using Task23BugLess.Models;

namespace Task23UnitTest
{
    [TestClass]
    public class QestionnaireControllerTest
    {
        [TestMethod]
        public void QuestionnaireController_CreateNewPost_ShouldNotReturnModel()
        {
            //arrange
            QuestionnaireController controller = new QuestionnaireController();
            //act
            var result = (controller.CreateNewPost() as ViewResult).Model;
            //assert
            Assert.IsNull(result);
        }
        [TestMethod]
        public void QuestionnaireController_CreateNewPost_ShouldAddBlog()
        {
            //arrange
            QuestionnaireController controller = new QuestionnaireController();
            var blogs = DeepCopyBlog(BlogModel.GetBlogs());
            Blog blogToAdd = new Blog { Id = 4, AuthorName = "Adam", Content = "Test", CreationTime = DateTime.Now, Header = "Nice day" };
            //act
            controller.CreateNewPost(blogToAdd);
            var actualBlogs = BlogModel.GetBlogs();
            //assert
            CollectionAssert.AreNotEqual(blogs, actualBlogs);
            CollectionAssert.Contains(actualBlogs, blogToAdd);
        }
        public List<Blog> DeepCopyBlog(List<Blog> blogList)
        {
            return blogList
               .ConvertAll(blog => new Blog
               {
                   Id = blog.Id,
                   AuthorName = blog.AuthorName,
                   Content = blog.Content,
                   Header = blog.Header,
                   CreationTime = blog.CreationTime
               });
        }
    }
}
