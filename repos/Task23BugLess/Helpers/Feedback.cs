﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task23BugLess.Helpers
{
    public struct Feedback
    {
        public string AuthorName { get; set; }
        public DateTime CreationTime { get; set; }
        public string Content { get; set; }
        public int BlogId { get; set; }
    }
}