﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Task23BugLess.Helpers
{
    public enum HelperType : short
    {
        Ul = 0,
        Ol = 1
    }
    public static class HtmlHelpers
    {
        public static MvcHtmlString CreateSelect(this HtmlHelper html, List<string> items, string name) 
        {
            var select = new TagBuilder("select");
            select.AddCssClass("form-control");
            select.MergeAttribute("name", name);
            foreach(var item in items)
            {
                var option = new TagBuilder("option");
                option.MergeAttribute("value", item);
                option.SetInnerText(item);
                select.InnerHtml += option.ToString();
            }
            return new MvcHtmlString(select.ToString());
        }
        public static MvcHtmlString CreateList(this HtmlHelper html, List<string> items, HelperType listType) 
        {
            StringBuilder output = new StringBuilder();
            var listTag = new TagBuilder( (Enum.GetName(typeof(HelperType), listType)).ToLower() );
            listTag.AddCssClass("questionnaire-rule-list");
            var liTag = new TagBuilder("li");

            foreach (var item in items)
            {
                liTag.SetInnerText(item);
                output.Append(liTag);
            }
            listTag.InnerHtml = output.ToString();
            output.Clear();
            output.Append(listTag.ToString());

            
            return new MvcHtmlString(output.ToString());
        }
    }
}